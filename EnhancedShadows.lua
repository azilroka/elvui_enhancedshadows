local AddOnName = ...
local E, L, V, P, G, _ = unpack(ElvUI)
local ES = E:NewModule('EnhancedShadows', 'AceEvent-3.0')
local EP = LibStub("LibElvUIPlugin-1.0")
local LSM = LibStub('LibSharedMedia-3.0')
local A = E:GetModule('Auras')
local ClassColor = RAID_CLASS_COLORS[E.myclass]
local Border, LastSize

ES.shadows = {}

function ES:GetOptions()
	E.Options.args.shadows = {
		type = "group",
		name = select(2, GetAddOnInfo(AddOnName)),
		get = function(info) return E.db.shadows[info[#info]] end,
		set = function(info, value) E.db.shadows[info[#info]] = value ES:UpdateShadows() end,
		args = {
			shadowcolor = {
				type = "color",
				order = 1,
				name = "Shadow Color",
				hasAlpha = false,
				get = function(info)
					local t = E.db.shadows[info[#info]]
					return t.r, t.g, t.b, t.a
				end,
				set = function(info, r, g, b)
					E.db.shadows[info[#info]] = {}
					local t = E.db.shadows[info[#info]]
					t.r, t.g, t.b = r, g, b
					ES:UpdateShadows()
				end,
			},
			classcolor = {
				type = 'toggle',
				order = 2,
				name = 'Class Color',
			},
			size = {
				order = 2,
				type = 'range',
				name = L["Size"],
				min = 1, max = 10, step = 1,
			},
		},
	}
end

-- Auras
A.UpdateAuraShadows = A.UpdateAura
function A:UpdateAura(button, index)
	self:UpdateAuraShadows(button, index)

	button:CreateShadow()
	ES:RegisterShadow(button.shadow)
end

P['shadows'] = { 
	['shadowcolor'] = { ['r'] = 0, ['g'] = 0, ['b'] = 0 },
	['classcolor'] = false,
	['size'] = 3,
}

function ES:ElvUIShadows()
	local Frames = {
		ElvUI_BottomPanel,
		ElvUI_TopPanel,
		ElvUI_ExperienceBar,
		ElvUI_ReputationBar,
		ElvUI_ConsolidatedBuffs,
		LeftMiniPanel,
		RightMiniPanel,
		GameTooltip,
		--LeftChatDataPanel,
		--LeftChatToggleButton,
		--RightChatDataPanel,
		--RightChatToggleButton,
		ElvConfigToggle,
	}

	local BackdropFrames = {
		ElvUIBags,
		ElvUI_BarPet,
		ElvUI_StanceBar,
		ElvUI_TotemBar,
		LeftChatPanel,
		RightChatPanel,
		Minimap,
		FarmModeMap,
		GameTooltipStatusBar,
	}

	for _, frame in pairs(Frames) do
		if frame then
			frame:CreateShadow()
			ES:RegisterShadow(frame.shadow)
		end
	end
	for _, frame in pairs(BackdropFrames) do
		if frame.backdrop then
			frame.backdrop:CreateShadow()
			ES:RegisterShadow(frame.backdrop.shadow)
		end
	end
	for i = 1, 10 do
		if _G["ElvUI_Bar"..i] then
			_G["ElvUI_Bar"..i].backdrop:CreateShadow()
			ES:RegisterShadow(_G["ElvUI_Bar"..i].backdrop.shadow)
			for k = 1, 12 do
				_G["ElvUI_Bar"..i.."Button"..k].backdrop:CreateShadow()
				ES:RegisterShadow(_G["ElvUI_Bar"..i.."Button"..k].backdrop.shadow)
			end
			_G["ElvUI_Bar"..i].backdrop:HookScript('OnShow', function(self) for k = 1, 12 do _G[self:GetParent():GetName().."Button"..k].backdrop.shadow:Hide() end end)
			_G["ElvUI_Bar"..i].backdrop:HookScript('OnHide', function(self) for k = 1, 12 do _G[self:GetParent():GetName().."Button"..k].backdrop.shadow:Show() end end)
			if _G["ElvUI_Bar"..i].backdrop:IsShown() then
				_G["ElvUI_Bar"..i].backdrop:Hide()
				_G["ElvUI_Bar"..i].backdrop:Show()
			else
				_G["ElvUI_Bar"..i].backdrop:Show()
				_G["ElvUI_Bar"..i].backdrop:Hide()
			end
		end
	end

	-- Stance Bar buttons
	for i = 1, NUM_STANCE_SLOTS do
		local stanceBtn = {_G["ElvUI_StanceBarButton"..i]}
		for _, button in pairs(stanceBtn) do
			if button then
				button:CreateShadow()
				ES:RegisterShadow(button.shadow)
			end
		end
	end
	_G["ElvUI_StanceBar"].backdrop:HookScript('OnShow', function(self) for i = 1, NUM_STANCE_SLOTS do _G[self:GetParent():GetName().."Button"..i].shadow:Hide() end end)
	_G["ElvUI_StanceBar"].backdrop:HookScript('OnHide', function(self) for i = 1, NUM_STANCE_SLOTS do _G[self:GetParent():GetName().."Button"..i].shadow:Show() end end)
	if _G["ElvUI_StanceBar"].backdrop:IsShown() then
		_G["ElvUI_StanceBar"].backdrop:Hide()
		_G["ElvUI_StanceBar"].backdrop:Show()
	else
		_G["ElvUI_StanceBar"].backdrop:Show()
		_G["ElvUI_StanceBar"].backdrop:Hide()
	end

	-- Unitframes (toDo Player ClassBars, Target ComboBar)
	local unitframes = {"Player", "Target", "TargetTarget", "Pet", "PetTarget", "Focus", "FocusTarget"}

	do
		for _, frame in pairs(unitframes) do 
			local self = _G["ElvUF_"..frame]
			local unit = string.lower(frame)
			local health = self.Health
			local power = self.Power
			local castbar = self.Castbar
			local portrait = self.Portrait

			health.backdrop:CreateShadow()
			ES:RegisterShadow(health.backdrop.shadow)
			if power then
				power.backdrop:CreateShadow()
				ES:RegisterShadow(power.backdrop.shadow)
			end
			if (unit == "player" or unit == "target" or unit == "focus") then
				if castbar then
					castbar.backdrop:CreateShadow()
					castbar.ButtonIcon.bg:CreateShadow()
					ES:RegisterShadow(castbar.backdrop.shadow)
					ES:RegisterShadow(castbar.ButtonIcon.bg.shadow)
				end
			end
			if (unit == "player" or unit == "target") then
				if portrait then
					portrait.backdrop:CreateShadow()
					ES:RegisterShadow(portrait.backdrop.shadow)
				end
			end
		end
	end

	LeftChatToggleButton:SetFrameStrata('BACKGROUND')
	LeftChatToggleButton:SetFrameLevel(LeftChatDataPanel:GetFrameLevel() - 1)
	RightChatToggleButton:SetFrameStrata('BACKGROUND')
	RightChatToggleButton:SetFrameLevel(RightChatDataPanel:GetFrameLevel() - 1)
end

function ES:UpdateShadows()
	if UnitAffectingCombat('player') then ES:RegisterEvent('PLAYER_REGEN_ENABLED', ES.UpdateShadows) return end

	for frame, _ in pairs(ES.shadows) do
		ES:UpdateShadow(frame)
	end
end

function ES:RegisterShadow(shadow)
	if shadow.isRegistered then return end
	ES.shadows[shadow] = true
	shadow.isRegistered = true
end

function ES:UpdateShadow(shadow)
	local ShadowColor = E.db.shadows.shadowcolor
	local r, g, b = ShadowColor['r'], ShadowColor['g'], ShadowColor['b']
	if E.db.shadows.classcolor then r, g, b = ClassColor['r'], ClassColor['g'], ClassColor['b'] end

	local size = E.db.shadows.size
	shadow:SetOutside(f, size, size)
	shadow:SetBackdrop({
		edgeFile = Border, edgeSize = E:Scale(size),
		insets = {left = E:Scale(5), right = E:Scale(5), top = E:Scale(5), bottom = E:Scale(5)},
	})
	shadow:SetBackdropColor(r, g, b, 0)
	shadow:SetBackdropBorderColor(r, g, b, 0.9)
end

function ES:Initialize()
	Border = LSM:Fetch('border', 'ElvUI GlowBorder')
	EP:RegisterPlugin(AddOnName, ES.GetOptions)
	ES:ElvUIShadows()
	ES:UpdateShadows()

	local timeToUpdate = .5;
	local frame = CreateFrame("Frame");
	frame.time = 0;
	frame:SetScript("OnUpdate", function(self, elapsed)
		if (frame.time + elapsed > timeToUpdate) then
			ES:UpdateShadows();
			frame.time = 0;
		else
			frame.time = frame.time + elapsed;
		end
	end)
end

E:RegisterModule(ES:GetName())